from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory, APITestCase

from ..factories import SprintFactory, UserFactory
from ..serializers import SprintSerializer, UserSerializer


class SprintSerializerTests(APITestCase):
    def setUp(self):
        factory = APIRequestFactory()
        url = reverse('sprint-detail', kwargs={'pk': 1})
        request = factory.get(url, format='json')
        self.context = {'request': request}

    def test_sprint_serializer_has_all_fields(self):
        sprint = SprintFactory()
        sprint_serializer = SprintSerializer(sprint, context=self.context)
        # TODO: Use mock.

        expected = {
                'id': sprint.pk,
                'name': sprint.name,
                'description': sprint.description,
                'end': sprint.end,
                'links': {
                    'self': 'http://testserver/api/sprints/1/',
                },
        }
        self.assertEqual(sprint_serializer.data, expected)


class UserSerializerTests(APITestCase):
    def test_user_serializer_has_desired_fields(self):
        user = UserFactory()
        user_serializer = UserSerializer(user)

        expected = {
            'id': user.id,
            'username': user.username,
            'full_name': user.get_full_name(),
            'is_active': user.is_active,
        }
        self.assertEquals(user_serializer.data, expected)
