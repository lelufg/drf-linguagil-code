import factory

from . import models


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.User

    username = factory.Sequence(lambda n: "User %03d" % n)
    email = factory.Faker('email')
    password = factory.Faker('password')
    is_active = True


class SprintFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Sprint

    name = factory.Faker('name')
    end = factory.Faker('date')
